package org.nrg.xnat.build.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildTestApplication {
	public static void main(final String[] arguments) {
		SpringApplication.run(BuildTestApplication.class, arguments);
	}
}
