package org.nrg.xnat.build.test.controllers;

import org.nrg.xnat.build.test.utils.BuildTestUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class BuildTestController {
    public BuildTestController() {
        setMessage("Default");
    }

    @GetMapping
    public String getTestMessage() {
        return _message;
    }

    @PostMapping
    public String setTestMessage(final @RequestParam String message) {
        return setMessage(message);
    }

    @DeleteMapping
    public String deleteTestMessage() {
        return setMessage("Deleted");
    }

    private String setMessage(final String message) {
        return (_message = message + " at " + BuildTestUtils.getTimestamp());
    }

    private String _message;
}
