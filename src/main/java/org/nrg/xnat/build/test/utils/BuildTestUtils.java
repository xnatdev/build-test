package org.nrg.xnat.build.test.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;

@Slf4j
public class BuildTestUtils {
    private BuildTestUtils() {
        log.debug("Can't build an instance of this");
    }

    public static String getTimestamp() {
        return Long.toString(CALENDAR.getTimeInMillis());
    }

    private static final Calendar CALENDAR = Calendar.getInstance();
}
